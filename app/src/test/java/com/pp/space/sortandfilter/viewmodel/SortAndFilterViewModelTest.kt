package com.pp.space.sortandfilter.viewmodel

import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnSortingOrderChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnSuccessChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnYearChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.ResetState
import com.pp.space.sortandfilter.viewmodel.mapper.FilterToViewState
import com.pp.usecases.SortingOrder.ASCENDING
import com.pp.usecases.SortingOrder.DESCENDING
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.filter.GetFilterUseCase.None
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test

class SortAndFilterViewModelTest {
    private val filter: Filter = Filter()
    private val getFilter: SynchronousUseCase<Filter, None> = mockk()
    private val filterToViewState: FilterToViewState = FilterToViewState()
    private lateinit var viewModel: SortAndFilterViewModel

    @Before
    fun setUp() {
        every { getFilter.invoke(None) } returns filter
        viewModel = SortAndFilterViewModel(getFilter, filterToViewState)
    }

    @Test
    fun `when starts then emits first filter`() {
        val testObserver = viewModel.viewStates.test()

        testObserver.assertValue(SortAndFilterViewState())
    }

    @Test
    fun `when OnSortingOrderChanged then changes success`() {
        viewModel.accept(OnSortingOrderChanged(sortingOrder = DESCENDING))

        val testObserver = viewModel.viewStates.test()

        testObserver.assertValue(SortAndFilterViewState(sortingOrder = DESCENDING))
    }

    @Test
    fun `when OnSuccessChanged then changes sorting order`() {
        viewModel.accept(OnSuccessChanged(true))

        val testObserver = viewModel.viewStates.test()

        testObserver.assertValue(
            SortAndFilterViewState(
                sortingOrder = ASCENDING,
                launchSuccessful = true
            )
        )
    }

    @Test
    fun `when OnYearChanged then changes year`() {
        viewModel.accept(OnYearChanged(YEAR))

        val testObserver = viewModel.viewStates.test()

        testObserver.assertValue(
            SortAndFilterViewState(
                sortingOrder = ASCENDING,
                year = YEAR
            )
        )
    }

    @Test
    fun `when ResetState then resets the state`() {
        viewModel.accept(OnYearChanged(YEAR))
        viewModel.accept(ResetState)

        val testObserver = viewModel.viewStates.test()

        testObserver.assertValue(SortAndFilterViewState())
    }

    private companion object {
        private const val YEAR = "year"
    }
}
