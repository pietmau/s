package com.pp.space.utils

import android.util.Log
import javax.inject.Inject

interface Logger {

    fun logException(tag: String, message: String?)
}

class AndroidLogger @Inject constructor() : Logger {

    override fun logException(tag: String, message: String?) {
        Log.e(tag, message)
    }
}
