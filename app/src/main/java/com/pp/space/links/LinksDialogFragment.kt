package com.pp.space.links

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.pp.space.databinding.FragmentLinksBinding
import com.pp.space.di.SpaceComponentProvider
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel.Links
import com.pp.space.utils.Logger
import com.pp.space.utils.visible
import javax.inject.Inject

class LinksDialogFragment : BottomSheetDialogFragment() {
    private lateinit var binding: FragmentLinksBinding

    @Inject
    lateinit var logger: Logger

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLinksBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val links = arguments?.getParcelable<Links>(LINKS)
        binding.article.visible = !links?.articleLink.isNullOrEmpty()
        binding.article.setOnClickListener {
            openLink(links?.articleLink)
        }
        binding.video.visible = !links?.videoLink.isNullOrEmpty()
        binding.video.setOnClickListener {
            openLink(links?.videoLink)
        }
        binding.wikipedia.visible = !links?.wikipediaLink.isNullOrEmpty()
        binding.wikipedia.setOnClickListener {
            openLink(links?.wikipediaLink)
        }
        binding.cancel.setOnClickListener {
            dismiss()
        }
        (activity as SpaceComponentProvider).spaceComponent.inject(this)
    }

    private fun openLink(link: String?) {
        link ?: return
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        if (requireContext().packageManager.queryIntentActivities(intent, 0).isNotEmpty()) {
            startActivity(intent)
        } else {
            logger.logException(
                LinksDialogFragment::class::simpleName.toString(),
                "No activity found to open uri"
            )
        }
    }

    companion object {
        fun newInstance(links: Links): LinksDialogFragment =
            LinksDialogFragment().apply {
                arguments = bundleOf(LINKS to links)
            }

        internal val TAG = LinksDialogFragment::class.simpleName!!

        private const val LINKS = "links"
    }
}
