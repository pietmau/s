package com.pp.space

import android.app.Application
import com.pp.space.di.ApplicationComponent
import com.pp.space.di.ApplicationComponentProvider
import com.pp.space.di.DaggerApplicationComponent

class SpaceApplication : Application(), ApplicationComponentProvider {
    override lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.create()
    }
}
