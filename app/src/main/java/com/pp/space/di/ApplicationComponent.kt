package com.pp.space.di

import com.pp.di.NetworkModule
import com.pp.repository.SpaceXService
import com.pp.space.utils.Logger
import com.pp.usecases.ObservableUseCase
import com.pp.usecases.Repository
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.filter.GetFilterUseCase
import com.pp.usecases.missions.MissionsListUseCase
import dagger.Component
import io.reactivex.subjects.BehaviorSubject

@ApplicationScope
@Component(modules = [ApplicationModule::class, NetworkModule::class])
interface ApplicationComponent {

    fun client(): SpaceXService

    fun repository(): Repository

    fun missionsUseCase(): ObservableUseCase<MissionsListUseCase.SpaceResponse, MissionsListUseCase.Params>

    fun missionsListViewModelFactoryProvider(): MissionsListViewModelFactoryProvider

    fun filterViewModelFactoryProvider(): FilterViewModelFactoryProvider

    fun subject(): BehaviorSubject<Filter>

    fun logger(): Logger

    fun filterUseCase(): SynchronousUseCase<Unit, Filter>

    fun getFilterUseCase(): SynchronousUseCase<Filter, GetFilterUseCase.None>
}

interface ApplicationComponentProvider {
    var applicationComponent: ApplicationComponent
}
