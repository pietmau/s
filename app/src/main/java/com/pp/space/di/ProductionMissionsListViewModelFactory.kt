package com.pp.space.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pp.space.missionslist.viewmodel.MissionsListViewModel
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.usecases.ObservableUseCase
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.missions.MissionsListUseCase.Params
import com.pp.usecases.missions.MissionsListUseCase.SpaceResponse

internal class ProductionMissionsListViewModelFactory(
    private val missionsUseCase: ObservableUseCase<SpaceResponse, Params>,
    private val mapper: (SpaceResponse) -> SpaceViewState,
    private val filtersUseCase: SynchronousUseCase<Unit, Filter>
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        MissionsListViewModel(
            missionsUseCase,
            mapper,
            filtersUseCase
        ) as T
}

interface MissionsListViewModelFactoryProvider {

    fun getMissionsListViewModelFactory(
        spaceUseCase: ObservableUseCase<SpaceResponse, Params>,
        mapper: (SpaceResponse) -> SpaceViewState,
        filtersUseCase: SynchronousUseCase<Unit, Filter>
    ): ViewModelProvider.Factory
}

class ProductionMissionsViewModelFactoryProvider : MissionsListViewModelFactoryProvider {

    override fun getMissionsListViewModelFactory(
        spaceUseCase: ObservableUseCase<SpaceResponse, Params>,
        mapper: (SpaceResponse) -> SpaceViewState,
        filtersUseCase: SynchronousUseCase<Unit, Filter>
    ): ViewModelProvider.Factory =
        ProductionMissionsListViewModelFactory(spaceUseCase, mapper, filtersUseCase)
}
