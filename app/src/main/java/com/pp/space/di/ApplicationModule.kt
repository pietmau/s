package com.pp.space.di

import com.pp.repository.InMemoryRepository
import com.pp.repository.SpaceXService
import com.pp.space.utils.AndroidLogger
import com.pp.space.utils.Logger
import com.pp.usecases.ObservableUseCase
import com.pp.usecases.Repository
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.filter.GetFilterUseCase
import com.pp.usecases.filter.GetFilterUseCase.None
import com.pp.usecases.filter.SaveFilterUseCase
import com.pp.usecases.missions.MissionsListUseCase
import com.pp.usecases.missions.MissionsListUseCase.Params
import com.pp.usecases.missions.MissionsListUseCase.SpaceResponse
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

@Module
abstract class ApplicationModule {

    @Binds
    abstract fun logger(androidLogger: AndroidLogger): Logger

    companion object {

        @Provides
        fun provideRepository(spaceXService: SpaceXService): Repository =
            InMemoryRepository(spaceXService)

        @SuppressWarnings("MaxLineLength")
        @Provides
        fun provideMissionsListUseCase(repository: Repository): ObservableUseCase<@JvmSuppressWildcards SpaceResponse, @JvmSuppressWildcards Params> =
            MissionsListUseCase(
                repository,
                Schedulers.io(),
                AndroidSchedulers.mainThread()
            )

        @ApplicationScope
        @Provides
        fun provideSubject(): BehaviorSubject<Filter> = BehaviorSubject.create()

        @Provides
        fun provideSaveFilterUseCase(
            subject: BehaviorSubject<Filter>
        ): SynchronousUseCase<@JvmSuppressWildcards Unit, @JvmSuppressWildcards Filter> =
            SaveFilterUseCase(subject)

        @Provides
        fun provideGetFilterUseCase(
            subject: BehaviorSubject<Filter>
        ): SynchronousUseCase<@JvmSuppressWildcards Filter, @JvmSuppressWildcards None> =
            GetFilterUseCase(subject)

        @Provides
        fun provideMissionsListViewModelFactoryProvider(): MissionsListViewModelFactoryProvider =
            ProductionMissionsViewModelFactoryProvider()

        @Provides
        fun provideFilterViewModelFactoryProvider(): FilterViewModelFactoryProvider =
            ProductionFilterViewModelFactoryProvider()
    }
}
