package com.pp.space.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewModel
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.filter.GetFilterUseCase.None

internal class ProductionFilterViewModelFactory(private val useCase: SynchronousUseCase<Filter, None>) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        SortAndFilterViewModel(useCase) as T
}

interface FilterViewModelFactoryProvider {

    fun getMissionsListViewModelFactory(
        useCase: SynchronousUseCase<Filter, None>
    ): ViewModelProvider.Factory
}

class ProductionFilterViewModelFactoryProvider : FilterViewModelFactoryProvider {
    override fun getMissionsListViewModelFactory(useCase: SynchronousUseCase<Filter, None>):
        ViewModelProvider.Factory = ProductionFilterViewModelFactory(useCase)
}
