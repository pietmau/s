package com.pp.space.di

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.pp.space.missionslist.viewmodel.MissionsListViewModel
import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent
import com.pp.space.missionslist.viewmodel.SpaceViewAction
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.space.missionslist.viewmodel.mapper.ResponseToViewState
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewModel
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewState
import com.pp.space.utils.DisposableViewModel
import com.pp.usecases.ObservableUseCase
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.filter.GetFilterUseCase
import com.pp.usecases.missions.MissionsListUseCase.Params
import com.pp.usecases.missions.MissionsListUseCase.SpaceResponse
import dagger.Module
import dagger.Provides

@Module
object SpaceModule {

    @Provides
    fun provideMapper(): Function1<@JvmSuppressWildcards SpaceResponse, @JvmSuppressWildcards SpaceViewState> =
        ResponseToViewState()

    @SuppressWarnings("MaxLineLength")
    @Provides
    fun provideMissionsListViewModel(
        activity: AppCompatActivity,
        spaceUseCase: ObservableUseCase<@JvmSuppressWildcards SpaceResponse, @JvmSuppressWildcards Params>,
        mapper: (@JvmSuppressWildcards SpaceResponse) -> @JvmSuppressWildcards SpaceViewState,
        missionsListViewModelFactoryProvider: MissionsListViewModelFactoryProvider,
        filtersUseCase: SynchronousUseCase<@JvmSuppressWildcards Unit, @JvmSuppressWildcards Filter>
    ): DisposableViewModel<@JvmSuppressWildcards SpaceViewState, @JvmSuppressWildcards SpaceViewAction, @JvmSuppressWildcards OneOffSpaceEvent> =
        ViewModelProvider(
            activity,
            missionsListViewModelFactoryProvider.getMissionsListViewModelFactory(
                spaceUseCase,
                mapper,
                filtersUseCase
            )
        )[MissionsListViewModel::class.java]

    @SuppressWarnings("MaxLineLength")
    @Provides
    fun provideSortAndFilterViewModel(
        activity: AppCompatActivity,
        filterViewModelFactory: FilterViewModelFactoryProvider,
        useCase: SynchronousUseCase<@JvmSuppressWildcards Filter, @JvmSuppressWildcards GetFilterUseCase.None>,
    ): DisposableViewModel<@JvmSuppressWildcards SortAndFilterViewState, @JvmSuppressWildcards SortAndFilterViewAction, @JvmSuppressWildcards Unit> =
        ViewModelProvider(
            activity,
            filterViewModelFactory.getMissionsListViewModelFactory(useCase)
        )[SortAndFilterViewModel::class.java]
}
