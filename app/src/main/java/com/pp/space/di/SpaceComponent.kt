package com.pp.space.di

import androidx.appcompat.app.AppCompatActivity
import com.pp.space.links.LinksDialogFragment
import com.pp.space.missionslist.view.MissionsListFragment
import com.pp.space.sortandfilter.view.SortAndFilterDialogFragment
import dagger.BindsInstance
import dagger.Component

@ActivityScope
@Component(modules = [SpaceModule::class], dependencies = [ApplicationComponent::class])
interface SpaceComponent {

    fun inject(missionsListFragment: MissionsListFragment)

    fun inject(sortAndFilterDialogFragment: SortAndFilterDialogFragment)

    fun inject(linksDialogFragment: LinksDialogFragment)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance activity: AppCompatActivity,
            applicationComponent: ApplicationComponent
        ): SpaceComponent
    }
}
