package com.pp.space

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pp.space.di.ApplicationComponentProvider
import com.pp.space.di.DaggerSpaceComponent
import com.pp.space.di.SpaceComponent
import com.pp.space.di.SpaceComponentProvider
import com.pp.space.missionslist.view.MissionsListFragment

class MainActivity : AppCompatActivity(), SpaceComponentProvider {
    override lateinit var spaceComponent: SpaceComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        spaceComponent = DaggerSpaceComponent.factory()
            .create(this, (application as ApplicationComponentProvider).applicationComponent)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MissionsListFragment.newInstance()).commit()
        }
    }
}
