package com.pp.space.sortandfilter.view.spinner

import android.R
import android.content.Context
import android.content.res.Resources.Theme
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatSpinner

class YearsSpinner @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = androidx.appcompat.R.attr.spinnerStyle,
    mode: Int = MODE_DIALOG,
    popupTheme: Theme? = null
) : AppCompatSpinner(context, attrs, defStyleAttr, mode, popupTheme) {

    private var years: List<String>? = null

    private lateinit var arrayAdapter: ArrayAdapter<String>

    private val year: String? = null

    fun bindYears(list: List<String>, year: String?) {
        bindYearsInternal(list)
        bindYear(year)
    }

    fun setListener(listener: (String?) -> Unit) {
        onItemSelectedListener = object : OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                /* NoOp */
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                listener(getYear())
            }
        }
    }

    private fun bindYearsInternal(years: List<String>) {
        if (this.years == years) {
            return
        }
        this.years = years
        this.arrayAdapter = ArrayAdapter(
            context, R.layout.simple_spinner_item,
            years
        )
        arrayAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
        adapter = arrayAdapter
    }

    private fun bindYear(year: String?) {
        if (this.year == year) {
            return
        }
        year?.let {
            setSelection(arrayAdapter.getPosition(it))
        }
    }

    internal fun getYear(): String? {
        return if (selectedItemPosition == 0) {
            null
        } else {
            getItemAtPosition(selectedItemPosition) as? String
        }
    }
}
