package com.pp.space.sortandfilter.view

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.pp.space.databinding.FragmentSortAndFilterBinding
import com.pp.space.di.SpaceComponentProvider
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnSortingOrderChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnSuccessChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnYearChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.ResetState
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewState
import com.pp.space.utils.DisposableViewModel
import com.pp.usecases.SortingOrder
import com.pp.usecases.SortingOrder.ASCENDING
import io.reactivex.disposables.Disposable
import kotlinx.parcelize.Parcelize
import javax.inject.Inject

class SortAndFilterDialogFragment : BottomSheetDialogFragment() {
    private lateinit var disposable: Disposable
    private var binding: FragmentSortAndFilterBinding? = null

    @SuppressWarnings("MaxLineLength")
    @Inject
    lateinit var sortAndFilterViewModel: DisposableViewModel<SortAndFilterViewState, SortAndFilterViewAction, Unit>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSortAndFilterBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as SpaceComponentProvider).spaceComponent.inject(this)
        binding?.cancel?.setOnClickListener {
            dismiss()
        }
        binding?.apply?.setOnClickListener {
            setFragmentResult(
                REQUEST_KEY,
                bundleOf(
                    VIEW_STATE to SearchParams(
                        year = binding?.spinner?.getYear(),
                        sortingOrder = requireNotNull(binding?.orderRadioGroup?.getSortingOrder()),
                        launchSuccessful = binding?.successfulUnsuccessfulRadioGroup?.getSuccessful()
                    )
                )
            )
            dismiss()
        }

        binding?.successfulUnsuccessfulRadioGroup?.setListener {
            sortAndFilterViewModel.accept(OnSuccessChanged(it))
        }
        binding?.orderRadioGroup?.setListener {
            sortAndFilterViewModel.accept(OnSortingOrderChanged(it))
        }
        binding?.spinner?.setListener {
            sortAndFilterViewModel.accept(OnYearChanged(it))
        }
        dialog?.setOnDismissListener {
            dismiss()
        }
    }

    override fun dismiss() {
        super.dismiss()
        sortAndFilterViewModel.accept(ResetState)
    }

    override fun onResume() {
        super.onResume()
        disposable = sortAndFilterViewModel.viewStates.subscribe(
            { viewState ->
                binding?.successfulUnsuccessfulRadioGroup?.bindSuccessfulUnsuccessful(viewState?.launchSuccessful)
                binding?.orderRadioGroup?.bindSortingOrder(viewState?.sortingOrder ?: ASCENDING)
                binding?.spinner?.bindYears(viewState?.years ?: emptyList(), viewState?.year)
            },
            {
                /* NoOp */
            }
        )
    }

    override fun onPause() {
        super.onPause()
        disposable.dispose()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    companion object {
        fun newInstance(): SortAndFilterDialogFragment = SortAndFilterDialogFragment()
        internal val TAG = SortAndFilterDialogFragment::class.simpleName!!
        internal const val VIEW_STATE = "view_state"
        internal const val REQUEST_KEY = "request_key"
    }

    @Parcelize
    data class SearchParams(
        val year: String? = null,
        val launchSuccessful: Boolean? = null,
        val sortingOrder: SortingOrder = ASCENDING
    ) : Parcelable
}
