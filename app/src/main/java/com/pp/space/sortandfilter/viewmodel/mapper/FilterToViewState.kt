package com.pp.space.sortandfilter.viewmodel.mapper

import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewState
import com.pp.usecases.filter.Filter

class FilterToViewState : (Filter) -> SortAndFilterViewState {

    override fun invoke(filter: Filter): SortAndFilterViewState = SortAndFilterViewState(
        year = filter.year,
        launchSuccessful = filter.launchSuccessful,
        sortingOrder = filter.sortingOrder,
        years = filter.years
    )
}
