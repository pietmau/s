package com.pp.space.sortandfilter.viewmodel

import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnSortingOrderChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnSuccessChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.OnYearChanged
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction.ResetState
import com.pp.space.sortandfilter.viewmodel.mapper.FilterToViewState
import com.pp.space.utils.DisposableViewModel
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.filter.GetFilterUseCase.None
import io.reactivex.Observable

class SortAndFilterViewModel(
    private val getFilter: SynchronousUseCase<Filter, None>,
    private val filterToViewState: FilterToViewState = FilterToViewState()
) :
    DisposableViewModel<SortAndFilterViewState, SortAndFilterViewAction, Unit>() {

    override val viewStates: Observable<SortAndFilterViewState>
        get() = Observable.just(filterToViewState(filter))

    private var filter: Filter = getFilter(None)

    override fun accept(viewAction: SortAndFilterViewAction) {
        filter = when (viewAction) {
            is OnSuccessChanged -> filter.copy(launchSuccessful = viewAction.successful)
            is OnSortingOrderChanged -> filter.copy(sortingOrder = viewAction.sortingOrder)
            is OnYearChanged -> filter.copy(year = viewAction.year)
            is ResetState -> getFilter(None)
        }
    }
}
