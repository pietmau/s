package com.pp.space.sortandfilter.view.radiogroups

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RadioGroup
import com.pp.space.databinding.SuccessfulUnsuccessfulRadioGroupBinding

class SuccessfulUnsuccessfulRadioGroup @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : RadioGroup(context, attrs) {

    private var binding: SuccessfulUnsuccessfulRadioGroupBinding =
        SuccessfulUnsuccessfulRadioGroupBinding.inflate(LayoutInflater.from(context), this)

    internal fun bindSuccessfulUnsuccessful(launchSuccessful: Boolean?) {
        if (launchSuccessful != null && launchSuccessful == getSuccessful()) {
            return
        }
        when (launchSuccessful) {
            true -> binding.successful.isChecked = true
            false -> binding.unsuccessful.isChecked = true
            else -> binding.both.isChecked = true
        }
    }

    internal fun setListener(listener: (Boolean?) -> Unit) {
        setOnCheckedChangeListener { _, _ -> listener(getSuccessful()) }
    }

    internal fun getSuccessful(): Boolean? =
        when {
            binding.successful.isChecked -> true
            binding.unsuccessful.isChecked -> false
            else -> null
        }
}
