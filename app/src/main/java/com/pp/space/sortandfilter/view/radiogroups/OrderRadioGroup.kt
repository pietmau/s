package com.pp.space.sortandfilter.view.radiogroups

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RadioGroup
import com.pp.space.databinding.OrderRadioGroupBinding
import com.pp.usecases.SortingOrder

class OrderRadioGroup @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) :
    RadioGroup(context, attrs) {

    private var binding: OrderRadioGroupBinding =
        OrderRadioGroupBinding.inflate(LayoutInflater.from(context), this)

    internal fun bindSortingOrder(sortingOrder: SortingOrder) {
        if (sortingOrder == getSortingOrderInternal()) {
            return
        }
        if (sortingOrder == SortingOrder.ASCENDING) {
            binding.ascending.isChecked = true
        } else if (sortingOrder == SortingOrder.DESCENDING) {
            binding.descending.isChecked = true
        }
    }

    internal fun getSortingOrder(): SortingOrder =
        getSortingOrderInternal() ?: SortingOrder.ASCENDING

    private fun getSortingOrderInternal(): SortingOrder? =
        when {
            binding.descending.isChecked -> SortingOrder.DESCENDING
            binding.ascending.isChecked -> SortingOrder.ASCENDING
            else -> null
        }

    fun setListener(listener: (SortingOrder) -> Unit) {
        setOnCheckedChangeListener { _, _ -> listener(getSortingOrder()) }
    }
}
