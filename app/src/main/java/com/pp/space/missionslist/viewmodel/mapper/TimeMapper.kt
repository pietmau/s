package com.pp.space.missionslist.viewmodel.mapper

import com.pp.space.utils.AndroidLogger
import com.pp.space.utils.Logger
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class TimeMapper(private val logger: Logger = AndroidLogger()) {
    private var dateFormat: SimpleDateFormat = SimpleDateFormat("dd MM yyyy 'at' HH:mm", Locale.UK)

    @SuppressWarnings("TooGenericExceptionCaught", "SwallowedException")
    fun dateToString(date: Int): String =
        try {
            dateFormat.format(Date(date.toLong() * MILLISECONDS_IN_SECOND))
        } catch (exception: Exception) {
            logger.logException(TimeMapper::class::simpleName.toString(), exception.message)
            INVALID_DATE
        }

    fun calculateDaysSinceOrFrom(now: Long, launchDateUnix: Int): Long =
        (now / MILLISECONDS_IN_SECOND - launchDateUnix) / (SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY)

    fun now(): Long = System.currentTimeMillis()

    fun getDaysLabel(now: Long, launchDateUnix: Int): String =
        if (now - launchDateUnix > 0) {
            SINCE
        } else {
            FROM
        }

    private companion object {
        private const val INVALID_DATE = "Invalid date"
        private const val SINCE = "Days since now:"
        private const val FROM = "Days from now:"
        private const val MILLISECONDS_IN_SECOND = 1000
        private const val SECONDS_IN_MINUTE = 60
        private const val MINUTES_IN_HOUR = 60
        private const val HOURS_IN_DAY = 24
    }
}
