package com.pp.space.missionslist.view.recycler

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pp.space.R
import com.pp.space.databinding.LaunchItemBinding
import com.pp.space.missionslist.viewmodel.SpaceViewState

class LaunchViewHolder(private val binding: LaunchItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        item: SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel,
        listener: (SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel.Links) -> Unit
    ) {
        binding.dateTime.text = item.launchDateTime
        binding.mission.text = item.missionName
        binding.rocket.text = item.rocket
        binding.dayLabel.text = item.daysLabel
        binding.days.text = item.days.toString()
        Glide.with(itemView.context).load(item.imageUrl).into(binding.image)
        if (item.successful) {
            Glide.with(itemView.context).load(R.drawable.ic_check_green).into(binding.check)
        } else {
            Glide.with(itemView.context).load(R.drawable.ic_cross_red).into(binding.check)
        }
        binding.root.setOnClickListener {
            listener(item.links)
        }
    }
}
