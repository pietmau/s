package com.pp.space.missionslist.view.recycler

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pp.space.missionslist.viewmodel.SpaceViewState

class LaunchesRecycler @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {
    private lateinit var listener: (SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel.Links) -> Unit

    private val launchesAdapter: LaunchesAdapter
        get() = adapter as LaunchesAdapter

    init {
        layoutManager = LinearLayoutManager(context)
        adapter = LaunchesAdapter { links: SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel.Links ->
            listener(links)
        }
    }

    fun bind(
        spaceLaunchesViewModel: SpaceViewState.Data.SpaceLaunchesViewModel,
        listener: (SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel.Links) -> Unit
    ) {
        // When filters are applied I think it's best if we scroll to the top.
        launchesAdapter.registerAdapterDataObserver(object : AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                if (itemCount != spaceLaunchesViewModel.launchViewModels.size) {
                    scrollToPosition(0)
                }
            }

            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                scrollToPosition(0)
            }

            override fun onItemRangeMoved(
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
                scrollToPosition(0)
            }
        })
        launchesAdapter.submitList(spaceLaunchesViewModel.launchViewModels)
        this.listener = listener
    }
}
