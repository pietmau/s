package com.pp.space.missionslist.viewmodel.mapper

import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.usecases.filter.Filter
import com.pp.usecases.missions.MissionsListUseCase.Params

class ParamsAndViewStateToFilter : (Params, SpaceViewState) -> Filter {

    override fun invoke(params: Params, spaceViewState: SpaceViewState): Filter =
        Filter(
            year = params.year,
            launchSuccessful = params.launchSuccessful,
            sortingOrder = params.sortingOrder,
            years = if (spaceViewState is SpaceViewState.Data) {
                val launches = spaceViewState.spaceLaunchesViewModel.launchViewModels
                listOf("All") + launches.map { it.year }.distinct()
            } else {
                emptyList()
            }
        )
}
