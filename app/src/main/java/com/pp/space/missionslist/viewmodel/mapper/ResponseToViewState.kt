package com.pp.space.missionslist.viewmodel.mapper

import com.pp.CompanyModel
import com.pp.LaunchModel
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceCompanyViewModel
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceLaunchesViewModel
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel
import com.pp.usecases.missions.MissionsListUseCase.SpaceResponse
import com.pp.usecases.missions.MissionsListUseCase.SpaceResponse.Data
import com.pp.usecases.missions.MissionsListUseCase.SpaceResponse.Error
import java.text.NumberFormat
import java.util.Locale

class ResponseToViewState(private val timeMapper: TimeMapper = TimeMapper()) : (SpaceResponse) -> SpaceViewState {
    private val numberFormat = NumberFormat.getNumberInstance(Locale.UK)

    override fun invoke(data: SpaceResponse): SpaceViewState =
        when (data) {
            is Data -> {
                SpaceViewState.Data(
                    companyViewModel = companyViewModel(data.companyInfo),
                    spaceLaunchesViewModel = launchesViewModel(data.launches)
                )
            }
            is Error -> SpaceViewState.Error(data.message)
        }

    private fun companyViewModel(companyInfo: CompanyModel): SpaceCompanyViewModel =
        SpaceCompanyViewModel(
            companyName = companyInfo.name,
            founderName = companyInfo.founder,
            employeesNumber = numberFormat.format(companyInfo.employees),
            launchSitesNumber = companyInfo.launchSites,
            valuation = numberFormat.format(companyInfo.valuation),
            yearFounded = companyInfo.founded
        )

    private fun launchesViewModel(launches: List<LaunchModel>): SpaceLaunchesViewModel {
        val now = timeMapper.now()
        return SpaceLaunchesViewModel(
            launches.map {
                LaunchViewModel(
                    flightNumber = it.flightNumber,
                    missionName = it.missionName,
                    launchDateTime = timeMapper.dateToString(it.launchDateUnix),
                    rocket = "${it.rocketName}/${it.rocketType}",
                    days = timeMapper.calculateDaysSinceOrFrom(now, it.launchDateUnix),
                    daysLabel = timeMapper.getDaysLabel(now, it.launchDateUnix),
                    imageUrl = it.imageUrl,
                    successful = it.successful,
                    year = it.year,
                    links = LaunchViewModel.Links(
                        articleLink = it.articleLink,
                        wikipediaLink = it.wikipediaLink,
                        videoLink = it.videoLink
                    )
                )
            }
        )
    }
}
