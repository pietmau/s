package com.pp.space.missionslist.viewmodel

import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent.OpenFilters
import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent.OpenLinksSelector
import com.pp.space.missionslist.viewmodel.SpaceViewAction.FilterApplied
import com.pp.space.missionslist.viewmodel.SpaceViewAction.OnFilterClicked
import com.pp.space.missionslist.viewmodel.SpaceViewAction.OnItemClicked
import com.pp.space.missionslist.viewmodel.mapper.ParamsAndViewStateToFilter
import com.pp.space.utils.DisposableViewModel
import com.pp.usecases.ObservableUseCase
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.missions.MissionsListUseCase.Params
import com.pp.usecases.missions.MissionsListUseCase.SpaceResponse
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class MissionsListViewModel(
    private val spaceUseCase: ObservableUseCase<SpaceResponse, Params>,
    private val responseToViewState: (SpaceResponse) -> SpaceViewState,
    private val saveFilterUseCase: SynchronousUseCase<Unit, Filter>,
    private val filterMapper: (Params, SpaceViewState) -> Filter = ParamsAndViewStateToFilter()
) : DisposableViewModel<SpaceViewState, SpaceViewAction, OneOffSpaceEvent>() {

    private val searchParamsSubject: Subject<Params> = PublishSubject.create()

    init {
        disposable = searchParamsSubject
            .startWith(Params())
            .flatMap { params ->
                spaceUseCase(params).map { response ->
                    responseToViewState(response)
                }.startWith(SpaceViewState.Loading)
                    .doOnNext { viewState ->
                        saveFilterUseCase(filterMapper(params, viewState))
                    }
            }
            .subscribe(
                {
                    viewStatesSubject.onNext(it)
                },
                { /* NoOp */ }
            )
    }

    override fun accept(viewAction: SpaceViewAction) =
        when (viewAction) {
            is FilterApplied -> onFilterApplied(viewAction)
            is OnFilterClicked -> oneOffEventsSubject.onNext(OpenFilters)
            is OnItemClicked -> oneOffEventsSubject.onNext(OpenLinksSelector(viewAction.links))
        }

    private fun onFilterApplied(viewAction: FilterApplied) {
        searchParamsSubject.onNext(
            Params(
                year = viewAction.year,
                launchSuccessful = viewAction.launchSuccessful,
                sortingOrder = viewAction.sortingOrder
            )
        )
    }
}
