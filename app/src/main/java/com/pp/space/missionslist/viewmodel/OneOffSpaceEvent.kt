package com.pp.space.missionslist.viewmodel

import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel.Links

sealed class OneOffSpaceEvent {

    object OpenFilters : OneOffSpaceEvent()

    data class OpenLinksSelector(val links: Links) : OneOffSpaceEvent()
}
