package com.pp.space.missionslist.viewmodel

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

sealed class SpaceViewState {

    object Loading : SpaceViewState()

    data class Data(
        val companyViewModel: SpaceCompanyViewModel,
        val spaceLaunchesViewModel: SpaceLaunchesViewModel
    ) : SpaceViewState() {

        data class SpaceCompanyViewModel(
            val companyName: String,
            val founderName: String,
            val employeesNumber: String,
            val launchSitesNumber: Int,
            val valuation: String,
            val yearFounded: String
        )

        data class SpaceLaunchesViewModel(val launchViewModels: List<LaunchViewModel>) {
            data class LaunchViewModel(
                val flightNumber: Int,
                val missionName: String,
                val launchDateTime: String,
                val rocket: String,
                val days: Long,
                val daysLabel: String,
                val imageUrl: String?,
                val successful: Boolean,
                val year: String,
                val links: Links
            ) {
                @Parcelize
                data class Links(
                    val articleLink: String?,
                    val videoLink: String?,
                    val wikipediaLink: String?
                ) : Parcelable {

                    val isEmpty
                        get() = articleLink == null && videoLink == null && wikipediaLink == null
                }
            }
        }
    }

    data class Error(val message: String) : SpaceViewState()
}
