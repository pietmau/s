package com.pp.space.missionslist.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceCompanyViewModel

class CompanyInfoView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    fun bind(companyInfoState: SpaceCompanyViewModel) {
        text = "${companyInfoState.companyName} was founded by ${companyInfoState.founderName} " +
            "in ${companyInfoState.yearFounded}. It has now " +
            "${companyInfoState.employeesNumber} employees," +
            " ${companyInfoState.launchSitesNumber} launch sites, and it's valued at USD" +
            " ${companyInfoState.valuation}"
    }
}
