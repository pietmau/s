package com.pp.space.missionslist.view.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.pp.space.databinding.LaunchItemBinding
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel.Links

class LaunchesAdapter(private val listener: (Links) -> Unit) :
    ListAdapter<LaunchViewModel, LaunchViewHolder>(DIFF_UTILS) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LaunchViewHolder =
        LaunchViewHolder(
            LaunchItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: LaunchViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    private companion object {
        private val DIFF_UTILS =
            object :
                DiffUtil.ItemCallback<LaunchViewModel>() {
                override fun areItemsTheSame(
                    oldItem: LaunchViewModel,
                    newItem: LaunchViewModel
                ): Boolean = oldItem.flightNumber == newItem.flightNumber

                override fun areContentsTheSame(
                    oldItem: LaunchViewModel,
                    newItem: LaunchViewModel
                ): Boolean = oldItem == newItem
            }
    }
}
