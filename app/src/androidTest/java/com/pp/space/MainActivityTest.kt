package com.pp.space

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.pp.space.missionslist.view.recycler.LaunchViewHolder
import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent
import com.pp.space.missionslist.viewmodel.SpaceViewAction.OnFilterClicked
import com.pp.space.missionslist.viewmodel.SpaceViewAction.OnItemClicked
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceCompanyViewModel
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceLaunchesViewModel
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel
import com.pp.space.missionslist.viewmodel.SpaceViewState.Data.SpaceLaunchesViewModel.LaunchViewModel.Links
import com.pp.space.missionslist.viewmodel.SpaceViewState.Error
import com.pp.space.utils.TestViewModel
import com.pp.space.utils.TestViewModel.clearTestViewModel
import com.pp.space.utils.TestViewModel.publishOneOffSpaceEvent
import com.pp.space.utils.TestViewModel.publishViewState
import com.pp.space.utils.atPosition
import com.pp.space.utils.launch
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {

    private val activityRule = object : ActivityTestRule<MainActivity>(MainActivity::class.java) {}

    @After
    fun tearDown() {
        clearTestViewModel()
    }

    @Test
    fun when_starts_shows_then_loading() {
        activityRule.launch()

        onView(withId(R.id.progress)).check(matches(isDisplayed()))
    }

    @Test
    fun when_gets_data_then_shows_item() {
        activityRule.launch()

        publishViewState(data)

        onView(withId(R.id.progress)).check(matches(not(isDisplayed())))
        onView(withId(R.id.launches_list)).check(
            matches(
                atPosition(0, hasDescendant(withText(MISSION_NAME)))
            )
        )
    }

    @Test
    fun when_gets_data_then_shows_company_info() {
        activityRule.launch()

        publishViewState(data)

        onView(withId(R.id.company_info)).check(matches(withText(containsString(COMPANY_NAME))))
    }

    @Test
    fun when_gets_error_then_shows_message() {
        activityRule.launch()

        publishViewState(Error(MESSAGE))

        onView(withId(R.id.progress)).check(matches(not(isDisplayed())))
        onView(withText(MESSAGE)).check(matches(isDisplayed()))
    }

    @Test
    fun when_receives_open_links_then_shows_links() {
        activityRule.launch()

        publishOneOffSpaceEvent(OneOffSpaceEvent.OpenLinksSelector(links))

        onView(withId(R.id.article)).check(matches(isDisplayed()))
    }

    @Test
    fun when_receives_show_filters_then_shows_filters() {
        activityRule.launch()

        publishViewState(data)
        publishOneOffSpaceEvent(OneOffSpaceEvent.OpenFilters)

        onView(withId(R.id.apply)).check(matches(isDisplayed()))
    }

    @Test
    fun when_click_item_then_sends_click_event() {
        activityRule.launch()

        publishViewState(data)

        onView(withId(R.id.launches_list))
            .perform(actionOnItemAtPosition<LaunchViewHolder>(0, click()))

        assertThat(TestViewModel.actions).hasAtLeastOneElementOfType(OnItemClicked::class.java)
    }

    @Test
    fun when_click_filter_then_sends_click_event() {
        activityRule.launch()

        publishViewState(data)

        onView(withId(R.id.filter_sort)).perform(click())

        assertThat(TestViewModel.actions).contains(OnFilterClicked)
    }

    private companion object {
        private const val MISSION_NAME = "mission_name"
        private const val COMPANY_NAME = "company_name"
        private const val LINK = "https://www.google.com"
        private const val MESSAGE = "message"

        private val companyViewModel = SpaceCompanyViewModel(
            companyName = COMPANY_NAME,
            founderName = "",
            employeesNumber = "",
            launchSitesNumber = 0,
            valuation = "",
            yearFounded = ""
        )
        private val links = Links(
            articleLink = LINK,
            videoLink = "",
            wikipediaLink = ""
        )

        private val launch = LaunchViewModel(
            flightNumber = 0,
            missionName = MISSION_NAME,
            launchDateTime = "",
            rocket = "",
            days = 0,
            daysLabel = "",
            imageUrl = "",
            successful = false,
            year = "",
            links = links
        )

        private val launchesState = SpaceLaunchesViewModel(listOf(launch))

        private val data = Data(companyViewModel, launchesState)
    }
}
