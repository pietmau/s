package com.pp.space.utils

import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewAction
import com.pp.space.sortandfilter.viewmodel.SortAndFilterViewState

object TestFilterViewModel :
    DisposableViewModel<SortAndFilterViewState, SortAndFilterViewAction, Unit>() {

    override fun accept(t: SortAndFilterViewAction?) {
        /* NoOp */
    }
}
