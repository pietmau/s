package com.pp.space.utils

import androidx.lifecycle.ViewModelProvider
import com.pp.space.di.FilterViewModelFactoryProvider
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.filter.GetFilterUseCase

class TestFilterViewModelFactoryProvider : FilterViewModelFactoryProvider {
    override fun getMissionsListViewModelFactory(useCase: SynchronousUseCase<Filter, GetFilterUseCase.None>): ViewModelProvider.Factory =
        TestFilterViewModelFactory()
}
