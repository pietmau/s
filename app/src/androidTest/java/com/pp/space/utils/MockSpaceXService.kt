package com.pp.space.utils

import com.pp.network.CompanyInfo
import com.pp.network.LaunchesItem
import com.pp.repository.SpaceXService
import io.reactivex.Observable

class MockSpaceXService : SpaceXService {

    override fun getCompanyInfo(): Observable<CompanyInfo> = Observable.empty()

    override fun getLaunches(): Observable<List<LaunchesItem>> = Observable.empty()
}
