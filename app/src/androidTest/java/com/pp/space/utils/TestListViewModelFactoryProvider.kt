package com.pp.space.utils

import androidx.lifecycle.ViewModelProvider
import com.pp.space.di.MissionsListViewModelFactoryProvider
import com.pp.space.missionslist.viewmodel.SpaceViewState
import com.pp.usecases.ObservableUseCase
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.missions.MissionsListUseCase

class TestListViewModelFactoryProvider : MissionsListViewModelFactoryProvider {

    override fun getMissionsListViewModelFactory(
        spaceUseCase: ObservableUseCase<MissionsListUseCase.SpaceResponse, MissionsListUseCase.Params>,
        mapper: (MissionsListUseCase.SpaceResponse) -> SpaceViewState,
        filtersUseCase: SynchronousUseCase<Unit, Filter>
    ): ViewModelProvider.Factory =
        TestMissionsListViewModelFactory()
}
