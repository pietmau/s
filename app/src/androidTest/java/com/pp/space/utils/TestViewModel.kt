package com.pp.space.utils

import com.pp.space.missionslist.viewmodel.OneOffSpaceEvent
import com.pp.space.missionslist.viewmodel.SpaceViewAction
import com.pp.space.missionslist.viewmodel.SpaceViewState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.TimeUnit

object TestViewModel : DisposableViewModel<SpaceViewState, SpaceViewAction, OneOffSpaceEvent>() {
    val actions: MutableList<SpaceViewAction> = mutableListOf()

    private val testViewStatesSubject: Subject<SpaceViewState> = PublishSubject.create()

    private val testViewOneOffSpaceEventSubject: Subject<OneOffSpaceEvent> = PublishSubject.create()

    override val viewStates: Observable<SpaceViewState>
        get() = testViewStatesSubject.observeOn(AndroidSchedulers.mainThread())

    override val oneOffEvents: Observable<OneOffSpaceEvent>
        get() = testViewOneOffSpaceEventSubject.throttleFirst(1, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())

    override fun accept(spaceViewAction: SpaceViewAction) {
        actions.add(spaceViewAction)
    }

    fun publishViewState(spaceViewState: SpaceViewState) {
        testViewStatesSubject.onNext(spaceViewState)
    }

    fun publishOneOffSpaceEvent(oneOffEvent: OneOffSpaceEvent) {
        testViewOneOffSpaceEventSubject.onNext(oneOffEvent)
    }

    fun clearTestViewModel() {
        actions.clear()
    }
}
