package com.pp.space.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TestFilterViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        TestFilterViewModel as T
}
