package com.pp.space.utils

import com.pp.repository.InMemoryRepository
import com.pp.repository.SpaceXService
import com.pp.space.di.ApplicationScope
import com.pp.space.di.FilterViewModelFactoryProvider
import com.pp.space.di.MissionsListViewModelFactoryProvider
import com.pp.usecases.ObservableUseCase
import com.pp.usecases.Repository
import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.Filter
import com.pp.usecases.filter.GetFilterUseCase
import com.pp.usecases.filter.SaveFilterUseCase
import com.pp.usecases.missions.MissionsListUseCase
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

@Module
object TestApplicationModule {

    @Provides
    fun provideLogger(): Logger = AndroidLogger()

    @Provides
    fun provideClient(): SpaceXService = MockSpaceXService()

    @Provides
    fun provideRepository(spaceXService: SpaceXService): Repository = InMemoryRepository(spaceXService)

    @Provides
    fun provideUseCase(repository: Repository): ObservableUseCase<MissionsListUseCase.SpaceResponse, MissionsListUseCase.Params> =
        MissionsListUseCase(
            repository,
            Schedulers.io(),
            AndroidSchedulers.mainThread()
        )

    @ApplicationScope
    @Provides
    fun provideSubject(): BehaviorSubject<Filter> = BehaviorSubject.create()

    @Provides
    fun provideMissionsListViewModelFactoryProvider(): MissionsListViewModelFactoryProvider =
        TestListViewModelFactoryProvider()

    @Provides
    fun provideFilterViewModelFactoryProvider(): FilterViewModelFactoryProvider =
        TestFilterViewModelFactoryProvider()

    @Provides
    fun provideSaveFilterUseCase(
        subject: BehaviorSubject<Filter>
    ): SynchronousUseCase<@JvmSuppressWildcards Unit, @JvmSuppressWildcards Filter> =
        SaveFilterUseCase(subject)

    @Provides
    fun provideGetFilterUseCase(
        subject: BehaviorSubject<Filter>
    ): SynchronousUseCase<@JvmSuppressWildcards Filter, @JvmSuppressWildcards GetFilterUseCase.None> =
        GetFilterUseCase(subject)
}
