package com.pp.space.utils

import com.pp.space.di.ApplicationComponent
import com.pp.space.di.ApplicationScope
import dagger.Component

@ApplicationScope
@Component(modules = [TestApplicationModule::class])
interface TestApplicationComponent : ApplicationComponent
