package com.pp.space.utils

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import org.hamcrest.Description
import org.hamcrest.Matcher

fun atPosition(position: Int, itemMatcher: Matcher<View?>): Matcher<View?>? {
    return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("has item at position $position: ")
            itemMatcher.describeTo(description)
        }

        override fun matchesSafely(view: RecyclerView): Boolean {
            val viewHolder =
                view.findViewHolderForAdapterPosition(position) ?: return false
            return itemMatcher.matches(viewHolder.itemView)
        }
    }
}

inline fun <reified T : Activity> ActivityTestRule<T>.launch(
    key: String? = null,
    extra: Int? = null
) {
    val context = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext
    val intent = Intent(context, T::class.java)
    if (key != null && extra != null) {
        intent.putExtra(key, extra)
    }
    launchActivity(intent)
}
