package com.pp

data class LaunchModel(
    val flightNumber: Int,
    val missionName: String,
    val rocketName: String,
    val rocketType: String,
    val launchDateUnix: Int,
    val imageUrl: String?,
    val successful: Boolean,
    val year: String,
    val articleLink: String?,
    val wikipediaLink: String?,
    val videoLink: String?
)
