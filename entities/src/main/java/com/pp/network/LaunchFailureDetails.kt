package com.pp.network

data class LaunchFailureDetails(
    val altitude: Int,
    val reason: String,
    val time: Int
)
