package com.pp.network

data class SecondStage(
    val block: Int,
    val payloads: List<Payload>
)
