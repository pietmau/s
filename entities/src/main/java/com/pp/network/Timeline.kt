package com.pp.network

@SuppressWarnings("ConstructorParameterNaming")
data class Timeline(
    val webcast_liftoff: Int
)
