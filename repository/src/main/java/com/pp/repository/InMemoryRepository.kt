package com.pp.repository

import com.pp.CompanyModel
import com.pp.LaunchModel
import com.pp.network.CompanyInfo
import com.pp.network.LaunchesItem
import com.pp.usecases.Repository
import io.reactivex.Observable

class InMemoryRepository(
    private val spaceXService: SpaceXService,
    private val companyInfoToCompanyModel: (CompanyInfo) -> CompanyModel = CompanyInfoToCompanyModel(),
    private val launchItemToLaunchModel: (LaunchesItem) -> LaunchModel = LaunchesItemToLaunchModel()
) : Repository {
    private var companyInfo: CompanyModel? = null
    private var launches: List<LaunchModel>? = null

    override fun getCompanyInfo(): Observable<CompanyModel> =
        companyInfo?.let { Observable.just(it) } ?: spaceXService.getCompanyInfo()
            .map { companyInfoToCompanyModel(it) }.doOnNext {
                companyInfo = it
            }

    override fun getLaunches(): Observable<List<LaunchModel>> =
        launches?.let { Observable.just(it) } ?: spaceXService.getLaunches()
            .flatMapIterable { it }
            .map { launchItemToLaunchModel(it) }
            .toList()
            .toObservable()
            .doOnNext {
                launches = it
            }
}
