package com.pp.repository

import com.pp.network.CompanyInfo
import com.pp.network.LaunchesItem
import io.reactivex.Observable

interface SpaceXService {

    fun getCompanyInfo(): Observable<CompanyInfo>

    fun getLaunches(): Observable<List<LaunchesItem>>
}
