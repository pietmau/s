package com.pp.repository

import com.pp.network.CompanyInfo
import com.pp.network.LaunchesItem
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test

class InMemoryRepositoryTest {
    private lateinit var repositoryIn: InMemoryRepository
    private val launch: LaunchesItem = mockk(relaxed = true)
    private val companyInfo: CompanyInfo = mockk(relaxed = true)
    private val XService: SpaceXService = mockk(relaxed = true) {
        every { getCompanyInfo() } returns Observable.just(companyInfo)
        every { getLaunches() } returns Observable.just(listOf(launch))
    }

    @Before
    fun setUp() {
        repositoryIn = InMemoryRepository(XService)
    }

    @Test
    fun `when company info is not cached the calls the client`() {
        repositoryIn.getCompanyInfo().test()

        verify { XService.getCompanyInfo() }
    }

    @Test
    fun `when launches info are not cached the calls the client`() {
        repositoryIn.getLaunches().test()

        verify { XService.getLaunches() }
    }

    @Test
    fun `when company info is cached does not call the client`() {
        repositoryIn.getCompanyInfo().test()
        repositoryIn.getCompanyInfo().test()

        verify(exactly = 1) { XService.getCompanyInfo() }
    }

    @Test
    fun `when launches are cached does not call the client`() {
        repositoryIn.getLaunches().test()
        repositoryIn.getLaunches().test()

        verify(exactly = 1) { XService.getLaunches() }
    }
}
