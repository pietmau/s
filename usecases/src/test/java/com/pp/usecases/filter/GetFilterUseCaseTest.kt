package com.pp.usecases.filter

import com.pp.usecases.filter.GetFilterUseCase.None
import io.mockk.every
import io.mockk.mockk
import io.reactivex.subjects.BehaviorSubject
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class GetFilterUseCaseTest {
    private val filter: Filter = mockk()
    private val filterSubject: BehaviorSubject<Filter> = mockk {
        every { value } returns filter
    }
    private val useCase = GetFilterUseCase(filterSubject)

    @Test
    fun `when a filter state is available then returns it`() {
        assertThat(useCase(None)).isEqualTo(filter)
    }
}
