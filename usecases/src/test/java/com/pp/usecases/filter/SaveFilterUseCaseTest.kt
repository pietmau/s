package com.pp.usecases.filter

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.subjects.BehaviorSubject
import org.junit.Test

class SaveFilterUseCaseTest {
    private val filter: Filter = Filter(years = listOf(TWENTY_TWENTYTWO))
    private val filterSubject: BehaviorSubject<Filter> = mockk(relaxed = true) {
        every { value } returns filter
    }
    private val saveFilterUseCase = SaveFilterUseCase(filterSubject)

    @Test
    fun `when filters does not have years then adds them`() {
        saveFilterUseCase(Filter())

        verify { filterSubject.onNext(Filter(years = listOf(TWENTY_TWENTYTWO))) }
    }

    @Test
    fun `when filters has years then does not add them`() {
        saveFilterUseCase(Filter(years = listOf(TWENTY_TWENTYONE)))

        verify { filterSubject.onNext(Filter(years = listOf(TWENTY_TWENTYONE))) }
    }

    private companion object {
        private const val TWENTY_TWENTYONE = "2021"
        private const val TWENTY_TWENTYTWO = "2022"
    }
}
