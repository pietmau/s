package com.pp.usecases.missions

import com.pp.CompanyModel
import com.pp.LaunchModel
import com.pp.usecases.ObservableUseCase
import com.pp.usecases.Repository
import com.pp.usecases.SortingOrder
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.functions.BiFunction

class MissionsListUseCase(
    private val repository: Repository,
    private val backgroundThreadScheduler: Scheduler,
    private val mainThreadScheduler: Scheduler
) : ObservableUseCase<MissionsListUseCase.SpaceResponse, MissionsListUseCase.Params> {

    override fun invoke(params: Params): Observable<SpaceResponse> {
        val launches = repository.getLaunches()
            .flatMapIterable { it }
            .filter { launch ->
                params.year?.equals(launch.year) ?: true
            }
            .filter { launch ->
                if (params.launchSuccessful == null) {
                    true
                } else {
                    params.launchSuccessful == launch.successful
                }
            }
            .sorted(LaunchesComparator(params.sortingOrder))
            .toList()
            .toObservable()

        val companyData = repository.getCompanyInfo()

        return Observable.zip(
            launches, companyData,
            BiFunction<List<LaunchModel>, CompanyModel, SpaceResponse> { launchesItems, company ->
                SpaceResponse.Data(
                    company,
                    launchesItems
                )
            }
        )
            .onErrorReturn {
                SpaceResponse.Error(it.message ?: ERROR)
            }
            .subscribeOn(backgroundThreadScheduler)
            .observeOn(mainThreadScheduler)
    }

    data class Params(
        val year: String? = null,
        val launchSuccessful: Boolean? = null,
        val sortingOrder: SortingOrder = SortingOrder.ASCENDING
    )

    sealed class SpaceResponse {

        data class Data(
            val companyInfo: CompanyModel,
            val launches: List<LaunchModel>
        ) : SpaceResponse()

        data class Error(val message: String) : SpaceResponse()
    }

    private companion object {
        private const val ERROR = "error"
    }
}
