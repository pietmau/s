package com.pp.usecases.missions

import com.pp.LaunchModel
import com.pp.usecases.SortingOrder
import com.pp.usecases.SortingOrder.ASCENDING

class LaunchesComparator(private val sortingOrder: SortingOrder) :
    Comparator<LaunchModel> {

    override fun compare(launchesItem0: LaunchModel, launchesItem1: LaunchModel): Int {
        val compared = launchesItem0.launchDateUnix - launchesItem1.launchDateUnix
        return if (sortingOrder == ASCENDING) {
            compared
        } else {
            -compared
        }
    }
}
