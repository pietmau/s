package com.pp.usecases.filter

import com.pp.usecases.SynchronousUseCase
import io.reactivex.subjects.BehaviorSubject

class SaveFilterUseCase(private val filterSubject: BehaviorSubject<Filter>) :
    SynchronousUseCase<Unit, Filter> {

    override fun invoke(params: Filter) {
        filterSubject.onNext(
            if (params.years.isEmpty()) {
                params.copy(years = filterSubject.value?.years ?: emptyList())
            } else {
                params
            }
        )
    }
}
