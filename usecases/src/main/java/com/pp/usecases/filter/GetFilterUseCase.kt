package com.pp.usecases.filter

import com.pp.usecases.SynchronousUseCase
import com.pp.usecases.filter.GetFilterUseCase.None
import io.reactivex.subjects.BehaviorSubject

class GetFilterUseCase(private val filterSubject: BehaviorSubject<Filter>) :
    SynchronousUseCase<Filter, None> {

    override fun invoke(params: None): Filter = requireNotNull(filterSubject.value)

    object None
}
