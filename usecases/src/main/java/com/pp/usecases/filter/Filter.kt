package com.pp.usecases.filter

import com.pp.usecases.SortingOrder

data class Filter(
    val year: String? = null,
    val launchSuccessful: Boolean? = null,
    val sortingOrder: SortingOrder = SortingOrder.ASCENDING,
    val years: List<String> = emptyList()
)
