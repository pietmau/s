package com.pp.usecases

interface SynchronousUseCase<out Response, in Params> {

    operator fun invoke(params: Params): Response
}
