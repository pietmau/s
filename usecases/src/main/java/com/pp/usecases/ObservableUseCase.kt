package com.pp.usecases

import io.reactivex.Observable

interface ObservableUseCase<out Response, in Params> {

    operator fun invoke(params: Params): Observable<out Response>
}
