package com.pp.usecases

enum class SortingOrder {
    ASCENDING, DESCENDING
}
