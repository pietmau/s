package com.pp.network

import com.pp.repository.SpaceXService
import io.reactivex.Observable
import retrofit2.Retrofit

class RetrofitSpaceXService(private val retrofit: Retrofit) : SpaceXService {

    private val api = retrofit.create(SpaceApi::class.java)

    override fun getCompanyInfo(): Observable<CompanyInfo> = api.getCompanyInfo()

    override fun getLaunches(): Observable<List<LaunchesItem>> = api.getLaunches()
}
