package com.pp.di

import com.pp.network.RetrofitSpaceXService
import com.pp.repository.SpaceXService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    fun provideSpaceService(): SpaceXService = RetrofitSpaceXService(getRetrofit())

    fun getRetrofit() = Retrofit.Builder()
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()

    private companion object {
        private const val BASE_URL = "https://api.spacexdata.com"
    }
}
