# Clean Architecture and MVVM Demo
This is a demo of a possible implementation of Clean architecture and MVVM. 

## Requirements

![Requirements](requirements.jpg)

## Libraries
- Kotlin, ViewModel, Retrofit, Mockk, Dagger, RxJava

### Modules
**1** - :app (Android module) 🤖

    depends on:
        - :entities
        - :usecases
        - :network
        - :repository
      
**2** - :network (Android module) 🤖

    depends on:
        - :entities
        - :repository

**3** - :repository (Kotlin only module) ☕
    
    depends on:
        - :entities
        - :usecases
        
**4** - :usecases (Kotlin only module) ☕
    
    depends on:
        - :entities
   
**5** - :entities (Kotlin only module) ☕

## Tests
:app

    - Unit tests
    - Espresso tests

:repository
    
    - Unit tests  

:usecases
    
    - Unit tests
